#!/usr/bin/python3

import enchant

wdlst = [ "Hallo", "tee", "moarn"]
dic = enchant.Dict("fy_NL")
for wd in wdlst:
    dic.check(wd)
    print("input word = {0}, Suggestions => {1}".format(wd, dic.suggest(wd)))
